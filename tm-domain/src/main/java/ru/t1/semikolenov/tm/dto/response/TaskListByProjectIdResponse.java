package ru.t1.semikolenov.tm.dto.response;

import lombok.Getter;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.Nullable;
import ru.t1.semikolenov.tm.model.Task;

import java.util.List;

@Getter
@NoArgsConstructor
public final class TaskListByProjectIdResponse extends AbstractResponse {

    @Nullable
    private List<Task> tasks;

    public TaskListByProjectIdResponse(@Nullable final List<Task> tasks) {
        this.tasks = tasks;
    }

}