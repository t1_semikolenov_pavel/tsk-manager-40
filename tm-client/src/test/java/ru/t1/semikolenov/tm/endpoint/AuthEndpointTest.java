package ru.t1.semikolenov.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.Assert;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.t1.semikolenov.tm.api.endpoint.IAuthEndpoint;
import ru.t1.semikolenov.tm.api.service.IPropertyService;
import ru.t1.semikolenov.tm.dto.request.UserLoginRequest;
import ru.t1.semikolenov.tm.dto.request.UserProfileRequest;
import ru.t1.semikolenov.tm.dto.response.UserLoginResponse;
import ru.t1.semikolenov.tm.dto.response.UserProfileResponse;
import ru.t1.semikolenov.tm.exception.field.EmptyLoginException;
import ru.t1.semikolenov.tm.exception.field.EmptyPasswordException;
import ru.t1.semikolenov.tm.exception.system.AccessDeniedException;
import ru.t1.semikolenov.tm.marker.ISoapCategory;
import ru.t1.semikolenov.tm.model.User;
import ru.t1.semikolenov.tm.service.PropertyService;

@Category(ISoapCategory.class)
public final class AuthEndpointTest {

    @NotNull
    private final IPropertyService propertyService = new PropertyService();

    @NotNull
    private final String host = propertyService.getServerHost();

    @NotNull
    private final String port = Integer.toString(propertyService.getServerPort());

    @NotNull
    private final IAuthEndpoint authEndpoint = IAuthEndpoint.newInstance(host, port);

    @Test
    public void login() {
        Assert.assertThrows(Exception.class,
                () -> authEndpoint.login(new UserLoginRequest("test", null)));
        Assert.assertThrows(Exception.class,
                () -> authEndpoint.login(new UserLoginRequest(null, null)));
        Assert.assertThrows(Exception.class,
                () -> authEndpoint.login(new UserLoginRequest("test_login", "1234")));
        Assert.assertThrows(Exception.class,
                () -> authEndpoint.login(new UserLoginRequest("test", "test_password")));
        @NotNull final UserLoginResponse response = authEndpoint.login(new UserLoginRequest("test", "test"));
        Assert.assertNotNull(response);
        Assert.assertTrue(response.getSuccess());
        Assert.assertNotNull(response.getToken());
    }

    @Test
    public void profile() {
        @NotNull final UserLoginResponse response = authEndpoint.login(new UserLoginRequest("test", "test"));
        @Nullable String token = response.getToken();
        Assert.assertThrows(Exception.class, () -> authEndpoint.profile(new UserProfileRequest()));
        Assert.assertThrows(Exception.class,
                () -> authEndpoint.profile(new UserProfileRequest("test_token")));
        UserProfileResponse responseProfile = authEndpoint.profile(new UserProfileRequest(token));
        Assert.assertNotNull(responseProfile);
        @Nullable User user = responseProfile.getUser();
        Assert.assertNotNull(user);
        Assert.assertEquals("test", user.getLogin());
    }

}
