package ru.t1.semikolenov.tm.command.system;

import org.jetbrains.annotations.NotNull;

public final class ExitCommand extends AbstractSystemCommand {

    @NotNull
    public static final String NAME = "exit";

    @NotNull
    public static final String DESCRIPTION = "Close application.";

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        System.exit(0);
    }

}